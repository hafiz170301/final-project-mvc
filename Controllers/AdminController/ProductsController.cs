﻿using FinalProject.Models;
using FinalProject.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FinalProject.Controllers.AdminController
{
    public class ProductsController : Controller
    {
        private readonly ProductInterface _product;
        private readonly CategoryInterface _category;

        public ProductsController(ProductInterface _product, CategoryInterface _category)
        {
            this._product = _product; 
            this._category = _category;
        }

        public async Task<IActionResult> Index()
        {
            List<Product> products = await _product.GetProductsAsync();
            List<Category> categories = await _category.GetCategoriesAsync();
            List<SelectListItem> dropdownItems = new List<SelectListItem>();

            foreach (var item in categories)
            {
                dropdownItems.Add(new SelectListItem
                {
                    Text = item.NamaKategori,
                    Value = item.IdKategori.ToString()
                });
            }

            ViewBag.Class = "Admin";
            ViewBag.CurrentTable = "Product";
            ViewBag.CategoriesDropdown = dropdownItems;

            return View(products);
        }

        [HttpGet]
        public async Task<JsonResult> GetProductById(Guid id)
        {
            Product product = await _product.GetProductAsync(id);

            return Json(product);
        }

        public async Task<IActionResult> CreateProduct(Product product)
        {
            await _product.CreateProduct(product);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UpdateProduct(Product product)
        {
            await _product.UpdateProduct(product);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteProduct(Guid id)
        {
            await _product.DeleteProduct(id);

            return RedirectToAction("Index");
        }
    }
}
