﻿using FinalProject.Models;
using FinalProject.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace FinalProject.Controllers.AdminController;

public class CategoriesController : Controller
{
    private readonly CategoryInterface _category;

    public CategoriesController(CategoryInterface _category)
    {
        this._category = _category;
    }

    public async Task<IActionResult> Index()
    {
        ViewBag.Class = "Admin";
        ViewBag.CurrentTable = "Product Type";

        List<Category> categories = await _category.GetCategoriesAsync();

        return View(categories);
    }

    [HttpGet]
    public async Task<JsonResult> GetCategoryById(Guid id)
    {
        Category category = await _category.GetCategoryAsync(id);

        return Json(category);
    }

    public async Task<IActionResult> CreateCategory(Category category)
    {
        await _category.CreateCategory(category);

        return RedirectToAction("Index");
    }

    public async Task<IActionResult> UpdateCategory(Category category)
    {
        await _category.UpdateCategory(category);

        return RedirectToAction("Index");
    }



    public async Task<IActionResult> DeleteCategory(Guid id)
    {
        await _category.DeleteCategory(id);

        return RedirectToAction("Index");
    }
}
