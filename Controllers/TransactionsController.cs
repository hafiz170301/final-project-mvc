﻿using FinalProject.Models;
using FinalProject.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace FinalProject.Controllers;

public class TransactionsController : Controller
{
    private readonly ProductInterface _product;
    private readonly TransactionInterface _transaction;

    public TransactionsController(ProductInterface _product, TransactionInterface _transaction)
    {
        this._product = _product;
        this._transaction = _transaction;
    }

    public async Task<IActionResult> Index()
    {
        List<Product> products = await _product.GetProductsAsync();

        ViewBag.Class = "Beli Barang";

        return View(products);
    }

    public async Task<IActionResult> CreateTransaction(int total, Product product)
    {
        var response = await _transaction.CreateTransaction(total, product);

        TempData["message"] = await response.Content.ReadAsStringAsync();
        TempData["statusCode"] = response.StatusCode;

        return RedirectToAction("Index");
    }
}
