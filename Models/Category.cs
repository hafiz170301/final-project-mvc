﻿namespace FinalProject.Models
{
    public class Category
    {
        public Guid IdKategori { get; set; }

        public string NamaKategori { get; set; } = null!;

        public virtual ICollection<Product> TabelBarangs { get; set; } = new List<Product>();
    }
}
