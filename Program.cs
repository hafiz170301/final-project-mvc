using FinalProject.Services;
using FinalProject.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddHttpClient<CategoryInterface, CategoryService>(val =>
val.BaseAddress = new Uri("https://localhost:7041/"));

builder.Services.AddHttpClient<ProductInterface, ProductService>(val =>
val.BaseAddress = new Uri("https://localhost:7041/"));

builder.Services.AddHttpClient<TransactionInterface, TransactionService>(val =>
val.BaseAddress = new Uri("https://localhost:7041/"));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
