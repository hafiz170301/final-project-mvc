﻿using FinalProject.Helpers;
using FinalProject.Models;
using FinalProject.Services.Interfaces;

namespace FinalProject.Services;

public class ProductService : ProductInterface
{
    private readonly HttpClient _client;

    public const string ProductPath = "/api/Products";

    public ProductService(HttpClient _client)
    {
        this._client = _client ?? throw new ArgumentNullException(nameof(_client));
    }

    public async Task<List<Product>> GetProductsAsync()
    {
        var response = await _client.GetAsync(ProductPath);

        return await response.ReadContentAsync<List<Product>>();
    }

    public async Task<Product> GetProductAsync(Guid id)
    {
        var response = await _client.GetAsync(ProductPath + "/" + id);

        return await response.ReadContentAsync<Product>();
    }

    public async Task CreateProduct(Product product)
    {
        await _client.PostAsJsonAsync(ProductPath, product);
    }

    public async Task UpdateProduct(Product product)
    {
        Product currentProduct = new Product();

        currentProduct.IdBarang = product.IdBarang;
        currentProduct.IdKategori = product.IdKategori;
        currentProduct.NamaBarang = product.NamaBarang;
        currentProduct.Deskripsi = product.Deskripsi;
        currentProduct.Stock = product.Stock;
        currentProduct.Harga = product.Harga;

        await _client.PutAsJsonAsync(ProductPath + "/" + product.IdBarang, currentProduct);
    }

    public async Task DeleteProduct(Guid id)
    {
        await _client.DeleteAsync(ProductPath + "/" + id);
    }
}
