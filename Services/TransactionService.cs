﻿using FinalProject.Models;
using FinalProject.Services.Interfaces;

namespace FinalProject.Services;

public class TransactionService : TransactionInterface
{
    private readonly HttpClient _client;

    public const string TransactionPath = "/api/Transactions";

    public TransactionService(HttpClient _client)
    {
        this._client = _client;
    }

    public async Task<HttpResponseMessage>CreateTransaction(int total, Product product)
    {
        var response = await _client.PostAsJsonAsync(TransactionPath + "?id=" + product.IdBarang + "&total=" + total, "");

        Console.WriteLine("Response From Transaction " + await response.Content.ReadAsStringAsync());
        Console.WriteLine("Status Code From Transaction " + response.StatusCode);

        return response;
    }
}
