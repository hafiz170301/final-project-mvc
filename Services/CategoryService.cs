﻿using FinalProject.Helpers;
using FinalProject.Models;
using FinalProject.Services.Interfaces;

namespace FinalProject.Services;

public class CategoryService : CategoryInterface
{
    private readonly HttpClient _client;

    public const string CategoryPath = "/api/Categories";

    public CategoryService(HttpClient _client)
    {
        this._client = _client ?? throw new ArgumentNullException(nameof(_client));
    }

    public async Task<List<Category>> GetCategoriesAsync()
    {
        var response = await _client.GetAsync(CategoryPath);

        return await response.ReadContentAsync<List<Category>>();
    }

    public async Task<Category> GetCategoryAsync(Guid id)
    {
        var response = await _client.GetAsync(CategoryPath + "/" + id);

        return await response.ReadContentAsync<Category>();
    }

    public async Task CreateCategory(Category category)
    {
        await _client.PostAsJsonAsync(CategoryPath, category);
    }

    public async Task UpdateCategory(Category category)
    {
        Category currentCategory = new Category();

        currentCategory.IdKategori = category.IdKategori;
        currentCategory.NamaKategori = category.NamaKategori;

        await _client.PutAsJsonAsync(CategoryPath + "/" + category.IdKategori, currentCategory);
    }

    public async Task DeleteCategory(Guid id)
    {
        await _client.DeleteAsync(CategoryPath + "/" + id);
    }
}
