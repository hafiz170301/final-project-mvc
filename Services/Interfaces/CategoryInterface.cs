﻿using FinalProject.Models;

namespace FinalProject.Services.Interfaces;

public interface CategoryInterface
{
    Task<List<Category>> GetCategoriesAsync();
    Task<Category> GetCategoryAsync(Guid id);
    Task CreateCategory(Category category);
    Task UpdateCategory(Category category);
    Task DeleteCategory(Guid id);
}
