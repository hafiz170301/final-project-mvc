﻿using FinalProject.Models;

namespace FinalProject.Services.Interfaces;

public interface ProductInterface
{
    Task<List<Product>> GetProductsAsync();
    Task<Product> GetProductAsync(Guid id);
    Task CreateProduct(Product product);
    Task UpdateProduct(Product product);
    Task DeleteProduct(Guid id);
}
