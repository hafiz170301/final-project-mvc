﻿using FinalProject.Models;

namespace FinalProject.Services.Interfaces;

public interface TransactionInterface
{
    Task<HttpResponseMessage> CreateTransaction(int total, Product product);
}
